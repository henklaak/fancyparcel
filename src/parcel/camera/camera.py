"""
Handles the communications with the camera.
"""
import logging
from typing import Union

from PySide6.QtCore import Signal, Slot, QObject, QSize
from PySide6.QtGui import QImage
from PySide6.QtMultimedia import QMediaDevices, QMediaCaptureSession, QCamera, \
    QImageCapture, QCameraFormat

logger = logging.getLogger(__name__)


class Camera(QObject):
    """
    A hardware camera is connected.
    The correct camera is found using the NAME_PATTERN
    """

    initialized = Signal()
    started = Signal()
    captured = Signal()
    error = Signal()

    NAME_PATTERN = 'UVC'

    def __init__(self, *args):
        super().__init__(*args)
        self._camera_device = None
        self._camera = None
        self._video_format = None
        self._capture_image = None
        self._initialized = False
        self._image = None
        self._capture_session = QMediaCaptureSession()

    def set_video_output(self, video_output: QObject):
        """
        Set the video output that the camera should send its live stream to

        Args:
            video_output: Arbitrary object that knows how to handle a video stream
        """
        self._capture_session.setVideoOutput(video_output)

    @property
    def extent(self) -> Union[None, QSize]:
        """
        Get the extent of the camera or None if not available.

        Returns:
            The extent of the camera or None if not available.
        """
        if not self._initialized:
            return None
        return self._camera.cameraFormat().resolution()

    def initialize(self):
        """ Initializes the camera """
        logger.debug("Camera initializing...")
        self._camera_device = self._find_camera()
        if not self._camera_device:
            return False

        self._video_format = self._pick_video_format()

        self._camera = QCamera(self._camera_device)
        self._camera.setCameraFormat(self._video_format)
        self._camera.setExposureMode(QCamera.ExposureMode.ExposureAuto)

        self._capture_session.setCamera(self._camera)

        self._capture_image = QImageCapture(self._camera)
        self._capture_image.imageCaptured.connect(self._on_image_captured)

        self._capture_session.setImageCapture(self._capture_image)
        self._initialized = True
        self.initialized.emit()

        logger.debug("Camera initialized")

    def terminate(self):
        """ Terminates the camera """
        logger.debug("Camera terminating...")
        self._initialized = False
        self._capture_image = None
        self._camera = None
        logger.debug("Camera terminated")

    @property
    def is_initialized(self):
        """ Returns true if the camera is initialized """
        return self._initialized

    def start(self):
        """ Start streaming video data """
        self._camera.start()
        logger.debug("Camera started")

    def stop(self):
        """ Stop streaming video data """
        self._camera.stop()
        logger.debug("Camera stopped")

    def capture(self):
        """ Capture a still image from the video stream """
        logger.debug("Camera capturing...")
        self._capture_image.capture()

    @property
    def captured_image(self) -> QImage:
        """ Returns the captured image """
        return self._image

    @Slot(int, QImage)
    def _on_image_captured(self, _, image):
        self._image = image
        self.captured.emit()
        image.save("bla.png")

        logger.debug("Camera captured")

    def _find_camera(self):
        video_inputs = QMediaDevices.videoInputs()
        if not video_inputs:
            return None

        video_input = [a for a in video_inputs if self.NAME_PATTERN in a.description()]
        if not video_input:
            return None

        return video_input[0]

    def _pick_video_format(self):

        supported_video_formats: list[QCameraFormat] = self._camera_device.videoFormats()
        video_format = supported_video_formats[2]  # Default
        max_pixels = video_format.resolution().width() * video_format.resolution().height()

        # See if we can a better alternative
        for svf in supported_video_formats:
            pixels = svf.resolution().width() * svf.resolution().height()
            if pixels > max_pixels:
                max_pixels = pixels
                video_format = svf

        return video_format
