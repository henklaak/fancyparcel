"""
Handles main application
"""
import logging
import sys

from PySide6.QtCore import SLOT, Slot
from PySide6.QtScxml import QScxmlStateMachine
from PySide6.QtWidgets import QApplication

from parcel.button import Button
from parcel.camera import Camera
from parcel.emailer import Emailer
from parcel.gui import MainWindow


class App(QApplication):
    """ Main pplication """

    def __init__(self, *args):
        super().__init__(*args)
        self.setStyle("plastique")

        # Create main components

        self.sm = QScxmlStateMachine.fromFile('parcel.scxml')
        self.button = Button()
        self.camera = Camera()
        self.emailer = Emailer()
        self.mw = MainWindow()

        # Connect events

        self.sm.connectToState("InitializeButton", self, SLOT("initialize_button_state(bool)"))
        self.sm.connectToState("InitializeCamera", self, SLOT("initialize_camera_state(bool)"))
        self.sm.connectToState("Live", self, SLOT("live_state(bool)"))
        self.sm.connectToState("Capture", self, SLOT("capture_state(bool)"))
        self.sm.connectToState("Frozen", self, SLOT("frozen_state(bool)"))
        self.sm.connectToState("Intervention", self, SLOT("intervention_state(bool)"))
        self.sm.connectToState("Transmit", self, SLOT("transmit_state(bool)"))
        self.sm.connectToState("Terminated", self, SLOT("terminated_state(bool)"))

        self.button.initialized.connect(lambda: self.sm.submitEvent("buttonInitialized"))
        self.button.triggered.connect(lambda: self.sm.submitEvent("captureStarted"))
        self.button.error.connect(lambda: self.sm.submitEvent("error"))

        self.camera.initialized.connect(lambda: self.sm.submitEvent("cameraInitialized"))
        self.camera.captured.connect(lambda: self.sm.submitEvent("captureDone"))
        self.camera.error.connect(lambda: self.sm.submitEvent("error"))

        self.emailer.sendComplete.connect(lambda: self.sm.submitEvent("transmitDone"))

        self.mw.reset_requested.connect(lambda: self.sm.submitEvent("reset"))
        self.mw.capture_requested.connect(lambda: self.sm.submitEvent("captureStarted"))
        self.mw.intervention_requested.connect(lambda: self.sm.submitEvent("interventionStarted"))
        self.mw.intervention_finished.connect(lambda: self.sm.submitEvent("interventionDone"))

        # Finalize

        # Tie camera video output to GUI video sink
        self.camera.set_video_output(self.mw.get_video_sink())

        self.mw.showMaximized()
        self.sm.init()
        self.sm.start()

    @Slot(bool)
    def initialize_button_state(self, entered):
        """ entered is True or False when entering/exiting """
        if entered:
            self.button.initialize()

    @Slot(bool)
    def initialize_camera_state(self, entered):
        """ entered is True or False when entering/exiting """
        if entered:
            self.camera.initialize()

    @Slot(bool)
    def live_state(self, entered):
        """ entered is True or False when entering/exiting """
        self.mw.enable_capture(entered)
        if entered:
            self.camera.start()
            self.mw.show_live(self.camera.extent)

    @Slot(bool)
    def capture_state(self, entered):
        """ entered is True or False when entering/exiting """
        if entered:
            self.camera.capture()

    @Slot(bool)
    def frozen_state(self, entered):
        """ entered is True or False when entering/exiting """
        self.mw.enable_intervention(entered)
        if entered:
            self.mw.show_captured_image(self.camera.captured_image)

    @Slot(bool)
    def intervention_state(self, entered):
        """ entered is True or False when entering/exiting """
        self.mw.enable_intervention_done(entered)

    @Slot(bool)
    def transmit_state(self, entered):
        """ entered is True or False when entering/exiting """
        if entered:
            self.emailer.send(self.mw.get_recipient(),
                              self.mw.get_metadata(),
                              self.camera.captured_image)

    @Slot(bool)
    def terminated_state(self, entered):
        """ entered is True or False when entering/exiting """
        self.mw.show_offline()
        self.mw.enable_reset(entered)
        if entered:
            self.camera.terminate()
            self.button.terminate()
            pass


def main():
    """ Main entry point"""
    logging.basicConfig(level=logging.DEBUG)
    app = App(sys.argv)
    return_code = app.exec()
    sys.exit(return_code)


if __name__ == '__main__':
    main()
