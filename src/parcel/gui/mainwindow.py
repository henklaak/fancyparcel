"""
Handles the graphical user interface.
"""

from PySide6.QtCore import Signal, QRectF, QSize, QObject
from PySide6.QtGui import QImage, QPixmap
from PySide6.QtMultimediaWidgets import QGraphicsVideoItem
from PySide6.QtWidgets import QMainWindow, QGraphicsScene, QGraphicsPixmapItem

from parcel.gui.ui_mainwindow import Ui_MainWindow


class MainWindow(QMainWindow, Ui_MainWindow):
    """
    Graphical user interface to see the live image, capture an image and intervene before sending the email.
    """
    reset_requested = Signal()
    capture_requested = Signal()
    intervention_requested = Signal()
    intervention_finished = Signal()

    def __init__(self, *args):
        super().__init__(*args)
        self.setupUi(self)

        self.pbReset.clicked.connect(self.reset_requested)
        self.pbCapture.clicked.connect(self.capture_requested)
        self.pbIntervene.clicked.connect(self.intervention_requested)
        self.pbInterventionDone.clicked.connect(self.intervention_finished)

        self._scene = QGraphicsScene()
        self.gvCamera.setScene(self._scene)
        self._video_sink = QGraphicsVideoItem()
        self._image_sink = QGraphicsPixmapItem()
        self._scene.addItem(self._image_sink)

    def enable_reset(self, enabled: bool):
        """
        Enable the possibility for the user to reset.

        Args:
            enabled: Reset is enabled
        """
        self.pbReset.setEnabled(enabled)

    def enable_capture(self, enabled: bool):
        """
        Enable the possibility for the user to capture.

        Args:
            enabled: Capture is enabled
        """
        self.pbCapture.setEnabled(enabled)

    def enable_intervention(self, enabled):
        """
        Enable the possibility for the user to intervene.

        Args:
            enabled: Intervene is enabled
        """
        self.pbIntervene.setEnabled(enabled)

    def enable_intervention_done(self, enabled: bool):
        """
        Enable the possibility for the user to finish the intervention.

        Args:
            enabled: Finish intervention is enabled
        """
        self.pbInterventionDone.setEnabled(enabled)

    def get_video_sink(self) -> QObject:
        """
        Get the live video sink widget

        Returns:
            An object that can handle a video stream
        """
        return self._video_sink

    def show_offline(self):
        """
        Configure the user interface for the camera offline case.
        """
        if self._video_sink in self._scene.items():
            self._scene.removeItem(self._video_sink)
        if self._image_sink in self._scene.items():
            self._scene.removeItem(self._image_sink)

    def show_live(self, extent: QSize):
        """
        Configure the user interface for the camera live case.

        Args:
            extent: Size of the video stream
        """
        self._video_sink.setSize(extent)

        r = QRectF(0, 0, extent.width(), extent.height())
        self.gvCamera.resetTransform()
        self.gvCamera.fitInView(r)
        self.gvCamera.setSceneRect(r)

        if self._image_sink in self._scene.items():
            self._scene.removeItem(self._image_sink)
        self._scene.addItem(self._video_sink)

    def show_captured_image(self, image: QImage):
        """
        Configure the user interface for the captured image case.

        Args:
            image: Captured image
        """
        r = QRectF(0, 0, image.width(), image.height())
        self.gvCamera.resetTransform()
        self.gvCamera.fitInView(r)
        self.gvCamera.setSceneRect(r)

        self._image_sink.setPixmap(QPixmap.fromImage(image))

        if self._video_sink in self._scene.items():
            self._scene.removeItem(self._video_sink)
        self._scene.addItem(self._image_sink)

    def get_recipient(self) -> str:
        """ Return recipient """
        return "info@laaksoft.nl"

    def get_metadata(self) -> str:
        """ Return metadata """
        return "No comment"
