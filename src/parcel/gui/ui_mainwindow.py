# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'mainwindow.ui'
##
## Created by: Qt User Interface Compiler version 6.5.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QGraphicsView, QHBoxLayout, QMainWindow,
    QMenuBar, QPushButton, QSizePolicy, QStatusBar,
    QVBoxLayout, QWidget)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(549, 344)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.horizontalLayout = QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.pbReset = QPushButton(self.centralwidget)
        self.pbReset.setObjectName(u"pbReset")
        self.pbReset.setEnabled(False)

        self.verticalLayout.addWidget(self.pbReset)

        self.pbCapture = QPushButton(self.centralwidget)
        self.pbCapture.setObjectName(u"pbCapture")
        self.pbCapture.setEnabled(False)

        self.verticalLayout.addWidget(self.pbCapture)

        self.pbIntervene = QPushButton(self.centralwidget)
        self.pbIntervene.setObjectName(u"pbIntervene")
        self.pbIntervene.setEnabled(False)

        self.verticalLayout.addWidget(self.pbIntervene)

        self.pbInterventionDone = QPushButton(self.centralwidget)
        self.pbInterventionDone.setObjectName(u"pbInterventionDone")
        self.pbInterventionDone.setEnabled(False)

        self.verticalLayout.addWidget(self.pbInterventionDone)


        self.horizontalLayout.addLayout(self.verticalLayout)

        self.gvCamera = QGraphicsView(self.centralwidget)
        self.gvCamera.setObjectName(u"gvCamera")

        self.horizontalLayout.addWidget(self.gvCamera)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 549, 22))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.pbReset.setText(QCoreApplication.translate("MainWindow", u"Reset", None))
        self.pbCapture.setText(QCoreApplication.translate("MainWindow", u"Capture", None))
        self.pbIntervene.setText(QCoreApplication.translate("MainWindow", u"Intervene", None))
        self.pbInterventionDone.setText(QCoreApplication.translate("MainWindow", u"Intervention done", None))
    # retranslateUi

