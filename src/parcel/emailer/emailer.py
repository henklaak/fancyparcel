"""
Handles emailing of a captured image with metadata.
"""
import logging

from PySide6.QtCore import Signal, QObject
from PySide6.QtGui import QImage

logger = logging.getLogger(__name__)


class Emailer(QObject):
    """
    Asynchronously sends an email with image and metadata
    """
    sendComplete = Signal()

    def __init__(self, parent=None):
        super().__init__(parent=parent)

    def send(self, recipient: str, comment: str, image: QImage):
        """
        Start sending an email to `recipient` with `comment` as body and `image` attached.
        When sending is complete, `sendComplete` will be emitted.

        Args:
            image: captured image
            recipient: e-mail address in user@domain form
            comment: Free text to include in e-mail message
        """
        logger.debug("Start sending e-mail")
        # TODO send the email
        logger.debug("Finished sending e-mail")

        self.sendComplete.emit()
