"""
Handles communications with the hardware button.
"""
import logging

from PySide6.QtCore import QObject, Signal, QTimer, Slot
from PySide6.QtSerialPort import QSerialPortInfo, QSerialPort

logger = logging.getLogger(__name__)

class Button(QObject):
    """
    A hardware button is connected to a serial port.
    It sends periodic characters to let us know that it is still alive.
    When the special '@' character comes in, the button was pressed.

    """
    initialized = Signal()
    triggered = Signal()
    error = Signal()

    BAUD_RATE = 115200
    TRIGGER = '@'
    WATCHDOG_TIMEOUT = 2000
    COM_PORT = "COM4"

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self._initialized = False
        self._serial_port = None
        self._watchdog_timer = QTimer(self)

    def initialize(self):
        """
        Start initialize the button.

        - Finds the correct serial port and connects it
        - Starts receiving data
        - Starts the watchdog to detect disconnection

        Emits `initialized` when initialization is successful, or `error` on error.

        """
        logger.debug("Button initializing...")

        serial_ports = QSerialPortInfo.availablePorts()
        if not serial_ports:
            logger.error("No serial ports found")
            self.error.emit()
            return

        selected_port = serial_ports[0]
        for serial_port in serial_ports:
            if  serial_port.portName() == self.COM_PORT:
                selected_port = serial_port

        self._serial_port = QSerialPort(selected_port)

        self._serial_port.setBaudRate(self.BAUD_RATE)
        self._serial_port.setParity(QSerialPort.Parity.NoParity)
        self._serial_port.setDataBits(QSerialPort.DataBits.Data8)
        self._serial_port.setStopBits(QSerialPort.StopBits.OneStop)

        self._serial_port.readyRead.connect(self._on_serial_port_ready_read)

        self._serial_port.open(QSerialPort.OpenModeFlag.ReadWrite)
        if not self._serial_port.isOpen():
            logger.error(f"Cannot open serial port {selected_port}")
            self.error.emit()
            return

        self._watchdog_timer.setSingleShot(True)
        self._watchdog_timer.timeout.connect(self._on_watchdog_timeout)
        self._watchdog_timer.start(self.WATCHDOG_TIMEOUT)

        self._initialized = True
        self.initialized.emit()
        logger.debug("Button initialized")

    def terminate(self):
        """
        Terminate the button. Stops the watchdog and closes the serial port.
        """
        logger.debug("Button terminating...")
        self._watchdog_timer.stop()
        self._initialized = False
        logger.debug("Button terminated...")

    @property
    def is_initialized(self):
        """ Returns true if successfully initialized. """
        return self._initialized

    @Slot()
    def _on_serial_port_ready_read(self):
        """ Callback to handle incoming serial port traffic. """
        while self._serial_port.bytesAvailable():
            data = self._serial_port.read(1)
            # Kick watchdog to prevent timeout
            self._watchdog_timer.start()
            if data == self.TRIGGER:
                logger.info("Button triggered")
                self.triggered.emit()

    @Slot()
    def _on_watchdog_timeout(self):
        """ Watchdog timeout, when the button has stopped sending data. """
        logger.error("Button watchdog timeout")
        self.terminate()
        self.error.emit()
