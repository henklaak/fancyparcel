from PySide6.QtScxml import QScxmlStateMachine


def test_statemachine(qtbot):
    sm = QScxmlStateMachine.fromFile('../src/parcel/parcel.scxml')
    sm.init()
    assert not sm.parseErrors()
    assert sm.isInitialized()
