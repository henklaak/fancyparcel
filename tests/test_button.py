from parcel.button import Button

DELAY = 500


def test_button_initialize(qtbot):
    btn = Button()
    assert not btn.is_initialized
    btn.initialize()
    assert btn.is_initialized
    btn.terminate()
    assert not btn.is_initialized


def test_watchdog_timeout(qtbot):
    btn = Button()
    btn.WATCHDOG_TIMEOUT = 1
    btn.initialize()
    assert btn.is_initialized
    qtbot.wait(10 * btn.WATCHDOG_TIMEOUT)
    assert not btn.is_initialized
