from PySide6.QtCore import QSize
from PySide6.QtGui import QImage
from PySide6.QtMultimediaWidgets import QVideoWidget

from parcel.camera import Camera

DELAY = 500


def test_camera_initialize(qtbot):
    cam = Camera()
    assert not cam.is_initialized
    assert cam.extent is None

    cam.initialize()
    assert cam.is_initialized
    assert cam.extent == QSize(1280, 720)

    cam.terminate()
    assert not cam.is_initialized
    assert cam.extent is None


def test_camera_grab(qtbot):
    cam = Camera()
    cam.initialize()
    cam.start()
    cam.capture()
    qtbot.waitSignal(cam.captured).wait()
    cam.stop()
    im: QImage = cam.captured_image
    assert isinstance(im, QImage)
    assert im.size() == QSize(1280, 720)
    im.save("bla.png")


def test_camera_live_output(qtbot):
    w = QVideoWidget()
    w.show()
    cam = Camera()
    cam.set_video_output(w)
    cam.initialize()
    cam.start()
    qtbot.waitSignal(w.videoSink().videoSizeChanged).wait()
    assert w.videoSink().videoSize() == QSize(1280, 720)
    cam.stop()


def test_camera_live_output_late_attach(qtbot):
    w = QVideoWidget()
    w.show()
    cam = Camera()
    cam.initialize()
    cam.start()
    qtbot.wait(DELAY)

    # Late attach output
    cam.set_video_output(w)

    qtbot.waitSignal(w.videoSink().videoSizeChanged).wait()
    assert w.videoSink().videoSize() == QSize(1280, 720)
    qtbot.wait(DELAY)
    cam.stop()
