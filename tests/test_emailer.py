from PySide6.QtGui import QImage
from PySide6.QtTest import QSignalSpy

from parcel.emailer import Emailer


def test_emailer(qtbot):
    emailer = Emailer()
    spy = QSignalSpy(emailer.sendComplete)
    emailer.send("info@laaksoft.nl", "No comment", QImage())
    assert spy.count()
